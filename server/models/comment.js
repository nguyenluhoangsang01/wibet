import { model, Schema } from "mongoose";

const commentSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    content: {
      type: String,
      required: true,
      trim: true,
    },
    picture: String,
  },
  { timestamps: true }
);

export default model("Comment", commentSchema);
