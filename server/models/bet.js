import { model, Schema } from "mongoose";

const betSchema = new Schema(
  {
    team: {
      type: Schema.Types.ObjectId,
      ref: "Team",
      required: true,
    },
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    match: {
      type: Schema.Types.ObjectId,
      ref: "Match",
      required: true,
    },
    money: {
      type: Number,
      default: 50,
    },
    betTime: String,
  },
  { timestamps: true }
);

export default model("Bet", betSchema);
