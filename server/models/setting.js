import { Schema, model } from "mongoose";

const settingSchema = new Schema({
  // Email

  // Password
  minPassword: {
    type: Number,
    default: 3,
  },
  maxPassword: {
    type: Number,
    default: 20,
  },
  wrongPasswordTimes: {
    type: Number,
    default: 5,
  },
  isMin: {
    type: Boolean,
    default: true,
  },
  isMax: {
    type: Boolean,
    default: true,
  },
  isUppercaseLetter: {
    type: Boolean,
    default: true,
  },
  isLowercaseLetter: {
    type: Boolean,
    default: true,
  },
  isNumber: {
    type: Boolean,
    default: true,
  },
  isSymbols: {
    type: Boolean,
    default: true,
  },

  // Rate
  minRate: {
    type: Number,
    default: 0,
  },
  maxRate: {
    type: Number,
    default: 3,
  },

  // Bet money
  minBetMoney: {
    type: Number,
    default: 50,
  },

  // Score
  maxScore: {
    type: Number,
    default: 10,
  },

  // Money
  defaultMoney: {
    type: Number,
    default: 200,
  },

  // Time to update score
  timeUpdateScore: {
    type: Number,
    default: 90,
  },

  // Time to bet
  timeBet: {
    type: Number,
    default: 5,
  },

  // Bank
  nameOfBank: {
    type: String,
	required: true,
	default: "Name of bank",
  },
  stkOfBank: {
    type: Number,
	required: true,
	default: "0123456789101",
  },
  bank: {
    type: String,
	required: true,
	default: "Bank name",
  },
  contentOfBank: {
    type: String,
	required: true,
	default: "Content of bank",
  },
  noteOfBank: {
    type: String,
	required: true,
	default: "Note of bank",
  },

  // MoMo
  numberOfMoMo: {
    type: Number,
	required: true,
	default: "0123456789",
  },
  nameOfMoMo: {
    type: String,
	required: true,
	default: "Name of MoMo",
  },
  contentOfMoMo: {
    type: String,
	required: true,
	default: "Content of MoMo",
  },
  noteOfMoMo: {
    type: String,
	required: true,
	default: "Note of MoMo",
  },

  // Skype
  skypeName: {
    type: String,
	required: true,
	default: "Skype name",
  },
  skypeLink: {
    type: String,
	required: true,
	default: "skype link",
  },
});

export default model("Setting", settingSchema);
