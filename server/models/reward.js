import { model, Schema } from "mongoose";

const rewardSchema = new Schema(
  {
    rewardName: {
      type: String,
      required: true,
      trim: true,
    },
    numberOfReward: {
      type: Number,
      required: true,
      trim: true,
    },
    rewardRate: {
      type: String,
      required: true,
      trim: true,
    },
  },
  { timestamps: true }
);

export default model("Reward", rewardSchema);
